use strict;
use warnings;
use Test::More;
use lib './t/lib';
use Pixeme::Test tests => 4;

BEGIN { use_ok("Pixeme") }

ok(!defined(eval{evaluate(<<EOF)}), 'let does not support recursion');
(let ((fac
        (lambda (n)
            (if n (* n (fac (- n 1)))
                1))))
    (fac 4))
EOF

is($@, "No value bound for fac in Pixeme::Env\n",
    'let does not support recursion [2]');

eval_ok(<<EOF, "24", 'letrec and recursion');
(letrec ((fac (lambda (n) (if n (* n (fac (- n 1))) 1)))) (fac 4))
EOF
