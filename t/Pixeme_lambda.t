use strict;
use warnings;
use Test::More;
use lib './t/lib';
use Pixeme::Test tests => 5;
use FileHandle;

BEGIN { use_ok('Pixeme') }

eval_ok(<<EOF, '16', "lambda");
(let ((square
            (lambda (x) (* x x))))
    (square 4))
EOF

eval_ok(<<EOF, '(lambda (x) (* x x))', 'lambda to_string');
(let ((square
            (lambda (x) (* x x))))
    square)
EOF

eval_ok(<<EOF, '12', 'closure');
(let ((times3
            (let ((n 3))
                (lambda (x) (* n x)))))
    (times3 4))
EOF

eval_ok(<<EOF, '15', 'higher order functions');
(let ((times3
            (let ((makemultiply
                        (lambda (n)
                            (lambda (x) (* n x)))))
                (makemultiply 3))))
    (times3 5))
EOF

