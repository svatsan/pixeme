use strict;
use warnings;
use Test::More;
use lib './t/lib';
use Pixeme::Test tests => 3;

BEGIN { use_ok('Pixeme') }

eval_ok(<<EOF, <<EOT, 'global definition');
(define square
    (lambda (x) (* x x)))
(square 4)
EOF
square
16
EOT

eval_ok(<<EOF, <<EOT, 'local definition');
(define times2
    (lambda (x)
        (begin
            (define h
                (lambda (k)
                    (- k (- k))))
            (h x))))
(times2 5)
EOF
times2
10
EOT
# vim: ft=perl
