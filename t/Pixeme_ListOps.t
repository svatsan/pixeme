use strict;
use warnings;
use Test::More;
use lib 't/lib';
use Pixeme::Test tests => 9;

BEGIN { use_ok("Pixeme") }

eval_ok(<<EOF, '(1 2 3)', 'list primitive');
(let ((a 1)
     (b 2)
     (c 3))
  (list a b c))
EOF

eval_ok(<<EOF, '1', 'car primitive');
(let ((a (list 1 2 3)))
    (car a))
EOF

eval_ok(<<EOF, '(2 3)', 'cdr primitive');
(let ((a (list 1 2 3)))
    (cdr a))
EOF

eval_ok(<<EOF, '(1 2 3)', 'cons primitive');
(cons 1 (list 2 3))
EOF

eval_ok('(quote (list 1 2 3))', '(list 1 2 3)', 'quote special form');

eval_ok('(quote (1 2 . 3))', '(1 2 . 3)', 'dot in');

eval_ok('(quote (1 2 . (3)))', '(1 2 3)', 'dot out');

eval_ok('(quote (1 . (2 . (3 . ()))))', '(1 2 3)', 'complex dot out');

