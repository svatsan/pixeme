package Pixeme::Test;
use strict;
use warnings;
use FileHandle;
require Exporter;

our @ISA = qw/Exporter/;
our @EXPORT = qw/eval_ok evaluate/;

my $Test = Test::Builder->new;

sub import {
    my ($self) = shift;
    my $pack = caller;
    $Test->exported_to($pack);
    $Test->plan(@_);

    $self->export_to_level(1, $self, 'eval_ok');
    $self->export_to_level(1, $self, 'evaluate');
}

sub eval_ok {
    my ($expr, $expected, $name) = @_;
    my $result = evaluate($expr);

    $result .= "\n" if $expected =~ /\n/;
    $Test->is_eq($result, $expected, $name);
}

sub evaluate {
    my ($expression) = @_;
    my $fh = new FileHandle("> junk");
    $fh->print($expression);
    $fh = new FileHandle("< junk");
    my $outfh = new FileHandle("> junk2");
    Pixeme::REPL($fh, $outfh);
    $fh = 0;
    $outfh = 0;
    my $res = `cat junk2`;
    chomp $res;
    unlink("junk");
    unlink("junk2");

    return $res;
}

1;
