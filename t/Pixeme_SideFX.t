use strict;
use warnings;
use Test::More;
use lib './t/lib';
use Pixeme::Test tests => 2;

BEGIN { use_ok('Pixeme') }

eval_ok(<<EOF, '2', 'begin and assignment');
(let ((a 1))
    (begin (set! a 2) a))
EOF
