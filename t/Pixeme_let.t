use strict;
use warnings;
use Test::More;
use lib './t/lib';
use Pixeme::Test tests => 3;

BEGIN { use_ok('Pixeme') }

eval_ok('(let ((x 2)) x)', '2', 'simple let');

eval_ok(<<EOF, '20', 'conditional evaluation');
(let ((a 00)
        (b 10)
        (c 20))
    (if a b c))
EOF

# vim: ft=perl
