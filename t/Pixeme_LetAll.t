use strict;
use warnings;
use Test::More;
use lib 't/lib';
use Pixeme::Test tests => 3;

BEGIN { use_ok("Pixeme") }

eval_ok(<<EOF, '1', 'let binds in parallel');
(let ((a 1)
      (b 2))
  (let ((a b)
       (b a))
      b))
EOF

eval_ok(<<EOF, '2', 'let* binds sequentially');
(let ((a 1)
      (b 2))
    (let* ((a b)
           (b a))
        b))
EOF
