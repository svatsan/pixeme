use strict;
use warnings;
use Test::More;

use lib './t/lib';
use Pixeme::Test tests => 10;

BEGIN { use_ok('Pixeme') }

eval_ok('1', '1', 'numbers');
eval_ok('+1', '1', 'explicit positive numbers');
eval_ok('-1', '-1', 'negative numbers');
eval_ok('"hello"', '"hello"', 'strings');
eval_ok('(* 2 3 4)', '24', 'multiplication');
eval_ok('(- 10 2 3)', '5', 'substraction');
eval_ok('(- 10)', '-10', 'negate operation');
eval_ok('(if (* 0 1) 10 20)', '20', 'simple conditional');
eval_ok(<<EOT, <<EOR, 'no overflow');
(* 1234567890987654321 1234567890987654321)
EOT
1524157877457704723228166437789971041
EOR

