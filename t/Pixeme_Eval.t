use strict;
use warnings;
use Test::More;
use lib './t/lib';
use Pixeme::Test tests => 3;
use FileHandle;

BEGIN { use_ok('Pixeme') }

eval_ok(<<EOT, <<EOR, 'eval');
(eval '(* 2 2))
EOT
4
EOR

eval_ok(<<EOT, <<EOR, 'eval operates in top-level');
(let ((* -))
    (eval '(* 3 3)))
EOT
9
EOR
