use strict;
use warnings;
use Test::More;
use lib './t/lib';
use Pixeme::Test tests => 4;

BEGIN { use_ok('Pixeme') }

#TODO: This doesn't pass currently. Check later.
#eval_ok(<<EOF, '(1 2)', 'macros');
#(let* ((mylet
#            (letrec ((map
#                        (lambda (op lst)
#                            (if lst
#                                (cons (op (car lst))
#                                    (map op (cdr lst)))
#                                ())))
#                    (cars
#                        (lambda (lst)
#                            (map car lst)))
#                    (cadrs
#                        (lambda (lst)
#                            (map (lambda (x) (car (cdr x))) lst))))
#                (macro (bindings body)
#                    (let* ((names (cars bindings))
#                            (values (cadrs bindings)))
#                        (cons (list (quote lambda) names body)
#                            values))))))
#    (mylet ((a 1)
#            (b 2))
#        (list a b)))
#EOF

eval_ok(<<EOF, <<EOR, 'unquote');
(let ((x (quote rain))
        (y (quote spain))
        (z (quote plain)))
    (quote (the (unquote x)
            in (unquote y)
            falls mainly on the
            (unquote z))))
EOF
(the rain in spain falls mainly on the plain)
EOR

eval_ok(<<EOF, <<EOR, 'quote and unquote syntactic sugar');
(let ((x 'rain)
        (y 'spain)
        (z 'plain))
    '(the ,x
        in ,y
        falls mainly on the 
        ,z))
EOF
(the rain in spain falls mainly on the plain)
EOR

eval_ok(<<EOF, <<EOR, 'macro to string');
(macro (x)
    '(a ,x))
EOF
(macro (x) (quote (a (unquote x))))
EOR

