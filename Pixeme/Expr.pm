package Pixeme::Expr;

use strict;
use warnings;
use base qw/Pixeme::Token/;

sub isTrue {
    my ($self) = @_;
    scalar($self->value);
}

sub is_null { 0 }
sub is_pair { 0 }
sub is_expr { 1 }

sub Eval {
    my ($self, $env) = @_;
    return $self;
}

sub map_eval {
    my ($self, $env) = @_;
    return $self->Eval($env);
}

sub value { $_[0] }
sub strings {
    my ($self) = @_;
    return ('.', $self->as_string);
}

sub Quote { $_[0] }
sub quote_rest { $_[0] }
sub is_unquote { 0 }

########################################
package Pixeme::Expr::Atom;
use base qw/Pixeme::Expr/;

sub new {
    my ($class, $value) = @_;
    bless \$value, $class;
}

sub value { ${ $_[0] } }

sub as_string { $_[0]->value }

########################################


########################################
package Pixeme::Expr::List;
use base qw/Pixeme::Expr/;

sub new {
    my ($class, @list) = @_;
    $class = ref($class) || $class;
    if(@list) {
        my $first = shift @list;
        $class->Cons($first, $class->new(@list));
    } else {
        new Pixeme::Expr::List::Null();
    }
}

sub Cons {
    my ($class, $first, $rest) = @_;
    return Pixeme::Expr::List::Pair->new($first, $rest);
}

sub as_string {
    my ($self) = @_;

    return '(' . join(' ', $self->strings) . ')';
}

########################################


########################################
package Pixeme::Expr::List::Pair;
use base qw/Pixeme::Expr::List/;

use constant {
    FIRST => 0,
    REST  => 1,
};

sub new {
    my ($class, $first, $rest) = @_;
    bless [$first, $rest], $class;
}

sub value {
    my ($self) = @_;
    my @value = ($self->[FIRST], $self->[REST]->value);
    return @value;
}

sub first { $_[0][FIRST] }
sub rest  { $_[0][REST]  }

sub strings {
    my ($self) = @_;
    return ($self->[FIRST]->as_string, $self->[REST]->strings);
}

sub Eval {
    my ($self, $env) = @_;
    my $op = $self->[FIRST]->Eval($env);
    return $op->Apply($self->[REST], $env);
}

sub map_eval {
    my ($self, $env) = @_;
    return $self->Cons($self->[FIRST]->Eval($env),
                       $self->[REST]->map_eval($env));
}

sub is_pair { 1 }
sub Quote {
    my ($self, $env) = @_;
    if($self->[FIRST]->is_unquote) {
        return $self->[REST]->first->Eval($env);
    } else {
        return $self->quote_rest($env);
    }
}

sub quote_rest {
    my ($self, $env) = @_;
    return $self->Cons($self->[FIRST]->Quote($env), $self->[REST]->quote_rest($env));
}

########################################  


########################################  
package Pixeme::Expr::List::Null;
use base qw/Pixeme::Expr::List/;

sub new {
    my ($class) = @_;
    $class =ref($class) || $class;
    bless {}, $class;
}

sub value { (); }

sub strings { (); }

sub first { $_[0] }

sub rest { $_[0] }

sub is_null { 1 }

########################################  


########################################  
package Pixeme::Expr::Symbol;
use base qw/Pixeme::Expr::Atom/;

sub Eval {
    my ($self, $env) = @_;
    return $env->lookUp($self);
}

sub is_symbol { 1 }
sub is_unquote {
    my ($self) = @_;
    return $self->value eq "unquote";
}
########################################  


########################################  
package Pixeme::Expr::Literal;
use base qw/Pixeme::Expr::Atom/;

########################################  


########################################  
package Pixeme::Expr::Number;
use base qw/Pixeme::Expr::Literal/;

use Math::BigInt;

sub new {
    my ($class, $value) = @_;
    $value = new Math::BigInt($value) unless ref($value);
    $class->SUPER::new($value);
}

########################################  


########################################  
package Pixeme::Expr::String;
use base qw/Pixeme::Expr::Literal/;

sub as_string {
    my ($self) = @_;

    my $copy = $self->value;
    $copy =~ s/\\/\\\\/sg;
    $copy =~ s!"!\"!sg;
    return qq'"$copy"';
}

########################################  

1;

