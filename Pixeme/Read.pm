package Pixeme::Read;;

use strict;
use warnings;

use Pixeme::Expr;
use Pixeme::Token;
use base qw/Pixeme/;

sub new {
    my ($class, $fh) = @_;
    bless {
        FileHandle => $fh,
        Line => '',
    }, $class;
}

sub Read {
    my ($self) = @_;

    my $token = $self->_next_token();
    return undef unless defined $token;

    if($token->is_quote_token) {
        my $expr = $self->Read;
        die "syntax error" unless defined($expr) && 
            $expr->is_expr;

        return new Pixeme::Expr::List(
            $token->is_unquote_token ?
                new Pixeme::Expr::Symbol('unquote') :
                new Pixeme::Expr::Symbol('quote'), $expr);
    }
    if($token->is_open_token) {
        return $self->read_list();
    } else {
        return $token;
    }
}

sub read_list {
    my ($self) = @_;
    my $token = $self->read_list_element();
    if($token->is_close_token) {
        return new Pixeme::Expr::List::Null();
    } elsif ($token->is_dot_token) {
        $token = $self->read_list_element();
        die "syntax error" unless $token->is_expr;
        my $close = $self->read_list_element();
        die "syntax error" unless $close->is_close_token;
        return $token;
    } else {
        return Pixeme::Expr::List->Cons($token, $self->read_list());
    }
}

sub read_list_element {
    my ($self) = @_;
    my $token = $self->Read();
    die "unexpected EOF" if !defined($token);
    return $token;
}

sub _next_token {
    my ($self) = @_;
    while(!$self->{Line}) {
        $self->{Line} = $self->{FileHandle}->getline();
        return undef unless defined $self->{Line};
        $self->{Line} =~ s/^\s+//s;
    }

    for($self->{Line}) {
        s/^\(\s*// && return Pixeme::Token::Open->new();
        s/^\)\s*// && return Pixeme::Token::Close->new();
        s/^([-+]?\d+)\s*// && return Pixeme::Expr::Number->new($1);
        s/^\.\s*// && return Pixeme::Token::Dot->new();
        s/^\'\s*// && return Pixeme::Token::Quote->new();
        s/^,\s*// && return Pixeme::Token::Unquote->new();
        s/^"((?:(?:\\.)|([^"]))*)"\s*// && do {
            my $string = $1;
            $string =~ s/\\//g;
            return Pixeme::Expr::String->new($string);
        };
        s/^([^\s\(\)]+)\s*// && return Pixeme::Expr::Symbol->new($1);
    }
    die "Can't parse: $self->{Line}\n";
}

1;
