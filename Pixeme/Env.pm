package Pixeme::Env;

use strict;
use warnings;
use base qw/Pixeme/;

sub new {
    my ($class, %bindings) = @_;
    $class = ref($class) || $class;
    bless { bindings => {%bindings}, parent => 0 }, $class;
}

sub Extend {
    my ($self, $symbols, $values) = @_;

    return $self->ExtendUnevald($symbols, $values->map_eval($self));
}

sub ExtendUnevald {
    my ($self, $symbols, $values) = @_;

    my %bindings;
    $self->_populate_bindings(\%bindings, $symbols, $values);
    my $newenv = $self->new(%bindings);
    $newenv->{parent} = $self;
    return $newenv;
}

sub ExtendRec {
    my ($self, $symbols, $values) = @_;

    my $newenv = $self->ExtendUnevald($symbols, $values);
    $newenv->_eval_values();
    return $newenv;
}

sub ExtendIter {
    my ($self, $symbols, $values) = @_;

    if($symbols->is_null) {
        return $self;
    } else {
        return $self->Extend($symbols->first, $values->first)->ExtendIter($symbols->rest, $values->rest);
    }
}

sub lookUp {
    my ($self, $symbol) = @_;

    if(defined(my $ref = $self->_lookup_ref($symbol))) {
        return $$ref;
    } else {
        die "No value bound for @{[$symbol->value]} in ", 
            @{[ ref($self) ]}, "\n";
    }
}

sub Assign {
    my ($self, $symbol, $value) = @_;
    if(defined(my $ref = $self->_lookup_ref($symbol))) {
        $$ref = $value;
    } else {
        die "No binding for @{[$symbol->value]} in @{[ ref($self) ]}\n";
    }
}

sub _lookup_ref {
    my ($self, $symbol) = @_;

    if(exists($self->{bindings}{ $symbol->value })) {
        return \$self->{bindings}{ $symbol->value };
    } elsif ($self->{parent}) {
        return $self->{parent}->_lookup_ref($symbol);
    } else {
        return undef;
    }
}

sub _eval_values {
    my ($self) = @_;
    map {
        $self->{bindings}{$_} = 
            $self->{bindings}{$_}->Eval($self)
    } keys %{ $self->{bindings} };
}

sub _populate_bindings {
    my ($self, $bindings, $symbols, $values) = @_;
    if($symbols->is_null) {
        die "Too many arguments\n" unless $values->is_null;
    } elsif ($symbols->is_pair) {
        if($values->is_pair) {
            my $symbol = $symbols->first;
            if($symbol->is_symbol) {
                $bindings->{$symbol->value} = $values->first;
                $self->_populate_bindings($bindings,
                    $symbols->rest,
                    $values->rest);
            } else {
                die "Bad formal arguments:", $symbol->as_string(), "\n";
            }
        } else {
            die "Not enough arguments\n";
        }
    } elsif ($symbols->is_symbol) {
        $bindings->{$symbols->value} = $values;
    } else {
        die "Bad formal arguments:", $symbols->as_string(), "\n";
    }
}

sub top {
    my ($self) = @_;
    if($self->{parent}) {
        return $self->{parent}->top;
    } else {
        return $self;
    }
}

sub Define {
    my ($self, $symbol, $value) = @_;

    $self->{bindings}{ $symbol->value } = $value;
    return $symbol;
}

1;
