package Pixeme::Closure;

use strict;
use warnings;
use base qw/Pixeme/;

sub new {
    my ($class, $args, $body, $env) = @_;
    bless {
        args => $args,
        body => $body,
        env => $env,
    }, $class;
}

sub args { $_[0]->{args}->value }
sub body { $_[0]->{body}        }
sub env  { $_[0]->{env}         }

sub _apply {
    my ($self, $args) = @_;

    my $extnd_env = $self->env->ExtendUnevald($self->{args}, $args);
    return $self->{body}->Eval($extnd_env);
}

sub as_string {
    my ($self) = @_;
    return Pixeme::Expr::List->new(
        $self->_symbol,
        $self->{args},
        $self->{body})->as_string;
}
########################################


########################################
package Pixeme::Closure::Function;
use base qw/Pixeme::Closure/;

sub Apply {
    my ($self, $form, $env) = @_;
    
    my @evald_args = $form->map_eval($env);
    return $self->_apply(@evald_args);
}

sub _symbol {
    Pixeme::Expr::Symbol->new('lambda');
}
########################################


########################################
package Pixeme::Closure::Macro;
use base qw/Pixeme::Closure/;

sub Apply {
    my ($self, $form, $env) = @_;

    my $new_form = $self->_apply($form);
    return $new_form->Eval($env);
}

sub _symbol {
    Pixeme::Expr::Symbol->new('macro');
}

########################################
1;
