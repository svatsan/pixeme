package Pixeme::SplForms;

use strict;
use warnings;
use base qw/Pixeme::Expr/;

########################################
package Pixeme::SplForms::If;
use base qw/Pixeme::SplForms/;

sub Apply {
    my ($self, $form, $env) = @_;

    my $cond = $form->first;
    my $truth = $form->rest->first;
    my $falsity = $form->rest->rest->first;

    if($cond->Eval($env)->isTrue) {
        return $truth->Eval($env);
    } else {
        return $falsity->Eval($env);
    }
}
########################################


########################################
package Pixeme::SplForms::Let;
use base qw/Pixeme::SplForms/;

sub Apply {
    my ($self, $form, $env) = @_;

    my ($symbols, $values, $body) = $self->unpack($form, $env);

    return $body->Eval($env->Extend($symbols, $values));
}

sub unpack {
    my ($self, $form, $env) = @_;

    my ($bindings, $body) = $form->value;
    my ($symbols, $values) = $self->unpack_bindings($bindings);
    return ($symbols, $values, $body);
}

sub unpack_bindings {
    my ($self, $bindings) = @_;
    if($bindings->is_null) {
        my $null = new Pixeme::Expr::List::Null();
        return ($null, $null);
    } else {
        my ($symbols, $values) = $self->unpack_bindings($bindings->rest);
        return (
            Pixeme::Expr::List->Cons($bindings->first->first, $symbols),
            Pixeme::Expr::List->Cons($bindings->first->rest->first, 
                $values));
    }
}

########################################

########################################
package Pixeme::SplForms::Lambda;
use base qw/Pixeme::SplForms/;

use Pixeme::Closure;

sub Apply {
    my ($self, $form, $env) = @_;

    my ($args, $body) = $form->value;
    return Pixeme::Closure::Function->new($args, $body, $env);
}
########################################


########################################
package Pixeme::SplForms::LetRec;
use base qw/Pixeme::SplForms::Let/;

sub Apply {
    my ($self, $form, $env) = @_;

    my ($symbols, $values, $body) = $self->unpack($form);

    return  $body->Eval(
        $env->ExtendRec($symbols, $values));
}
########################################


########################################
package Pixeme::SplForms::LetAll;
use base qw/Pixeme::SplForms::Let/;

sub Apply {
    my ($self, $form, $env) = @_;

    my ($symbols, $values, $body) = $self->unpack($form);
    return $body->Eval($env->ExtendIter($symbols, $values));
}
########################################


########################################
package Pixeme::SplForms::Quote;
use base qw/Pixeme::SplForms/;

sub Apply {
    my ($self, $form, $env) = @_;
    return $form->first->Quote($env);
}
########################################


########################################
package Pixeme::SplForms::Macro;
use base qw/Pixeme::SplForms/;

sub Apply {
    my ($self, $form, $env) = @_;
    my ($args, $body) = $form->value;
    return Pixeme::Closure::Macro->new($args, $body,$env);
}

########################################


########################################
package Pixeme::SplForms::Eval;
use base qw/Pixeme::SplForms/;

sub Apply {
    my ($self, $form, $env) = @_;
    return $form->first->Eval($env)->Eval($env->top);
}

########################################


########################################
package Pixeme::SplForms::Set;
use base qw/Pixeme::SplForms/;

sub Apply {
    my ($self, $form, $env) = @_;
    my ($symbol, $expr) = $form->value;
    $env->Assign($symbol, $expr->Eval($env));
}

########################################


########################################
package Pixeme::SplForms::Begin;
use base qw/Pixeme::SplForms/;

sub Apply {
    my ($self, $form, $env) = @_;
    my (@expressions) = $form->value;
    my $ret = new Pixeme::Expr::List();

    while(@expressions) {
        $ret = shift(@expressions)->Eval($env);
    }
    return $ret;
}

########################################


########################################
package Pixeme::SplForms::Define;
use base qw/Pixeme::SplForms/;

sub Apply {
    my ($self, $form, $env) = @_;
    my ($symbol, $expr) = $form->value;
    $env->Define($symbol, $expr->Eval($env));
}

########################################
1;
