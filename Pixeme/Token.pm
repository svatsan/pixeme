package Pixeme::Token;

use strict;
use warnings;
use base qw/Pixeme/;

sub is_open_token  { 0 }
sub is_close_token { 0 }
sub is_dot_token   { 0 }
sub is_quote_token { 0 }
sub is_unquote_token { 0 }
sub is_expr         { 0 }

########################################
package Pixeme::Token::Open;
use base qw/Pixeme::Token/;

sub is_open_token { 1 }
########################################


########################################
package Pixeme::Token::Close;
use base qw/Pixeme::Token/;

sub is_close_token { 1 }
########################################


########################################
package Pixeme::Token::Dot;
use base qw/Pixeme::Token/;

sub is_dot_token { 1 }

########################################


########################################
package Pixeme::Token::Quote;
use base qw/Pixeme::Token/;

sub is_quote_token { 1 }

########################################


########################################
package Pixeme::Token::Unquote;
use base qw/Pixeme::Token::Quote/;

sub is_unquote_token { 1 }

########################################
1;
