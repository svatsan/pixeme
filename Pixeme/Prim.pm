package Pixeme::Prim;

use strict;
use warnings;
use base qw/Pixeme::Expr/;

sub Apply {
    my ($self, $form, $env) = @_;

    my @evaled_args = $form->map_eval($env)->value();
    return $self->_apply(@evaled_args);
}

sub _check_type {
    my ($self, $thing, $type) = @_;
    die "Wrong type of argumnet(", ref($thing), ") to (", ref($self), "\n"
    unless $thing->isa($type);
}


########################################
package Pixeme::Prim::Mult;
use base qw/Pixeme::Prim/;

sub _apply {
    my ($self, @args) = @_;
    my $result = Pixeme::Expr::Number->new(1)->value();

    while(@args) {
        my $arg = shift @args;
        $self->_check_type($arg, 'Pixeme::Expr::Number');
        $result *= $arg->value;
    }

    return new Pixeme::Expr::Number($result);
}
########################################


########################################
package Pixeme::Prim::Subtract;
use base qw/Pixeme::Prim/;

sub _apply {
    my ($self, @args) = @_;

    unshift @args, Pixeme::Expr::Number->new(0) if @args < 2;
    my $arg = shift @args;
    $self->_check_type($arg, 'Pixeme::Expr::Number');

    my $result = $arg->value;
    while(@args) {
        $arg = shift @args;
        $self->_check_type($arg, 'Pixeme::Expr::Number');
        $result -= $arg->value;
    }
    
    return new Pixeme::Expr::Number($result);
}
########################################

########################################
package Pixeme::Prim::List;
use base qw/Pixeme::Prim/;

sub _apply {
    my ($self, @args) = @_;

    return new Pixeme::Expr::List(@args);
}
########################################


########################################
package Pixeme::Prim::Car;
use base qw/Pixeme::Prim/;

sub _apply {
    my ($self, $arg) = @_;

    $self->_check_type($arg, 'Pixeme::Expr::List');
    return $arg->first;
}
########################################


########################################
package Pixeme::Prim::Cdr;
use base qw/Pixeme::Prim/;

sub _apply {
    my ($self, $arg) = @_;

    $self->_check_type($arg, 'Pixeme::Expr::List');
    return $arg->rest;
}
########################################


########################################
package Pixeme::Prim::Cons;
use base qw/Pixeme::Prim/;

sub _apply {
    my ($slef, $car, $cdr) = @_;
    return Pixeme::Expr::List->Cons($car, $cdr);
}

########################################


1;
