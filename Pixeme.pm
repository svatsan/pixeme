#!/usr/bin/perl

package Pixeme;
use strict;
use warnings;
use Pixeme::Read;
use Pixeme::Env;
use Pixeme::Prim;
use Pixeme::SplForms;
use FileHandle;

require Exporter;
our @ISA = qw/Exporter/;
our @EXPORT = qw/REPL/;

our $GLOBALS = new Pixeme::Env(
    '*' => new Pixeme::Prim::Mult(),
    '-' => new Pixeme::Prim::Subtract(),
    'if' => new Pixeme::SplForms::If(),
);

sub REPL {
    my ($ifh, $ofh) = @_;
    $ofh ||= new FileHandle(">-");

    my $reader = new Pixeme::Read($ifh);
    my $initial_env = new Pixeme::Env(
            '*'     => new Pixeme::Prim::Mult(),
            '-'     => new Pixeme::Prim::Subtract(),
            'list'  => new Pixeme::Prim::List(),
            'car'   => new Pixeme::Prim::Car(),
            'cdr'   => new Pixeme::Prim::Cdr(),
            'cons'  => new Pixeme::Prim::Cons(),
            'if'    => new Pixeme::SplForms::If(),
            'let'   => new Pixeme::SplForms::Let(),
            'lambda'=> new Pixeme::SplForms::Lambda(),
            'letrec'=> new Pixeme::SplForms::LetRec(),
            'let*'  => new Pixeme::SplForms::LetAll(),
            'quote' => new Pixeme::SplForms::Quote(),
            'macro' => new Pixeme::SplForms::Macro(),
            'eval'  => new Pixeme::SplForms::Eval(),
            'set!'  => new Pixeme::SplForms::Set(),
            'begin' => new Pixeme::SplForms::Begin(),
            'define' => new Pixeme::SplForms::Define(),
    );
    while(defined(my $expr = $reader->Read)) {
        my $result = $expr->Eval($initial_env);
        $result->Print($ofh);
    }
}

sub Print {
    my ($self, $ofh) = @_;
    print $ofh $self->as_string, "\n";
}

sub as_string { ref($_[0]); }

sub new { bless {}, $_[0]; }

1;
